/* Original Author 2008-2012: J.Rios
 * 
 * Edited by: 2012-2013 Jörn Magens <shuerhaaken@googlemail.com>
 * 
 * 
 * This Program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

#include "taginfo.h"
#include "taginfo_internal.h"



using namespace TagInfo;


FlacInfo::FlacInfo(const string &filename) : Info(filename) {
    if(!file_name.isEmpty() && !create_file_ref())
        printf("Error creating file ref! %s\n", filename.c_str());
    if(taglib_file) {// && !taglib_file->isNull()) 
        m_XiphComment = ((TagLib::FLAC::File *) taglib_file)->xiphComment();
    }
    else {
        printf("Cant get xiphcomment from '%s'\n", file_name.toCString(false));
        m_XiphComment = NULL;
    }
}


FlacInfo::~FlacInfo() {
}


bool FlacInfo::create_file_ref() {
    if(file_name.isEmpty())
        return false;
    taglib_file = new TagLib::FLAC::File(file_name.toCString(false), true, TagLib::AudioProperties::Fast);
    if(taglib_file) {
        return true;
    }
    else {
        printf("TagLib::File creation failed for '%s'\n", file_name.toCString(false));
        return false;
    }
}


bool FlacInfo::read(void) {
    if(Info::read()) {
        if(m_XiphComment) {
            
            read_virtual_tags((TagLib::Tag *)m_XiphComment);
            
            if(m_XiphComment->fieldListMap().contains("COMPOSER")) {
                composer = m_XiphComment->fieldListMap()["COMPOSER"].front();
            }
            if(m_XiphComment->fieldListMap().contains("DISCNUMBER")) {
                disk_str = m_XiphComment->fieldListMap()["DISCNUMBER"].front();
            }
            if(m_XiphComment->fieldListMap().contains("COMPILATION")) {
                is_compilation = m_XiphComment->fieldListMap()["COMPILATION"].front() == String("1");
            }
            if(m_XiphComment->fieldListMap().contains("ALBUMARTIST")) {
                album_artist = m_XiphComment->fieldListMap()["ALBUMARTIST"].front();
            }
            else if(m_XiphComment->fieldListMap().contains("ALBUM ARTIST")) {
                album_artist = m_XiphComment->fieldListMap()["ALBUM ARTIST"].front();
            }
            // Rating
            if(m_XiphComment->fieldListMap().contains("RATING")) {
                long Rating = 0;
                Rating = atol(m_XiphComment->fieldListMap()["RATING"].front().toCString(true));
                if(Rating) {
                    if(Rating > 5) {
                        rating = popularity_to_rating(Rating);
                    }
                    else {
                        rating = Rating;
                    }
                }
            }
            if(m_XiphComment->fieldListMap().contains("PLAY_COUNTER")) {
                long PlayCount = 0;
                PlayCount = atol(m_XiphComment->fieldListMap()["PLAY_COUNTER"].front().toCString(false));
                playcount = PlayCount;
            }
            // Labels
            if(track_labels.size() == 0) {
                if(m_XiphComment->fieldListMap().contains("TRACK_LABELS")) {
                    track_labels_str = m_XiphComment->fieldListMap()["TRACK_LABELS"].front();
                    track_labels = split(track_labels_str, "|");
                }
            }
            if(artist_labels.size() == 0) {
                if(m_XiphComment->fieldListMap().contains("ARTIST_LABELS")) {
                    artist_labels_str = m_XiphComment->fieldListMap()["ARTIST_LABELS"].front();
                    artist_labels = split(artist_labels_str, "|");
                }
            }
            if(album_labels.size() == 0) {
                if(m_XiphComment->fieldListMap().contains("ALBUM_LABELS")) {
                    album_labels_str = m_XiphComment->fieldListMap()["ALBUM_LABELS"].front();
                    album_labels = split(album_labels_str, "|");
                }
            }
            if(m_XiphComment->contains("COVERART")) {// TODO
                has_image = true;
            }
            else {
                TagLib::FLAC::File * fi = ((TagLib::FLAC::File *) taglib_file);
                List<TagLib::FLAC::Picture *> plist = fi->pictureList();
                has_image = (plist.size() > 0);
            }
            return true;
        }
    }
    return false;
}



bool FlacInfo::write(void) {
    if(m_XiphComment) {
        if(changedflag & CHANGED_DATA_TAGS) {
            m_XiphComment->addField("DISCNUMBER", disk_str);
            m_XiphComment->addField("COMPOSER", composer);
            
            if(is_compilation)
                m_XiphComment->addField("COMPILATION", String("1"));
            else
                m_XiphComment->addField("COMPILATION", String("0"));
            
            m_XiphComment->addField("ALBUMARTIST", album_artist);
            write_virtual_tags((TagLib::Tag *)m_XiphComment);
        }
        if(changedflag & CHANGED_DATA_RATING) {
            m_XiphComment->addField("RATING", format("%u", rating_to_popularity(rating)).c_str());
            m_XiphComment->addField("PLAY_COUNTER", format("%u", playcount).c_str());
        }
        if(changedflag & CHANGED_DATA_LABELS) {
            // The Labels
            check_xiph_label_frame(m_XiphComment, "ARTIST_LABELS", artist_labels_str);
            check_xiph_label_frame(m_XiphComment, "ALBUM_LABELS", album_labels_str);
            check_xiph_label_frame(m_XiphComment, "TRACK_LABELS", track_labels_str);
        }
    }
    return Info::write();
}


bool FlacInfo::can_handle_images(void) {
    return true;
}

bool FlacInfo::get_image(char*& data, int &data_length, ImageType &image_type) {
    data = NULL;
    data_length = 0;
    image_type = IMAGE_TYPE_UNKNOWN;
    
    get_xiph_comment_cover_art(m_XiphComment, data, data_length, image_type);
    if(!(data) || (data_length <= 0)) {
        TagLib::FLAC::File * fi = ((TagLib::FLAC::File *) taglib_file);
        List<TagLib::FLAC::Picture *> plist = fi->pictureList();
        TagLib::FLAC::Picture * result_pic = NULL;
        for(List<TagLib::FLAC::Picture *>::Iterator it = plist.begin(); it != plist.end(); ++it) {
            TagLib::FLAC::Picture::Type ty = (*it)->type();
            switch((*it)->type()) {
                case TagLib::FLAC::Picture::FrontCover:
                    result_pic = *it;
                    break;
                default:
                    break;
            }
        }
        if(!result_pic) {
            for(List<TagLib::FLAC::Picture *>::Iterator it = plist.begin(); it != plist.end(); ++it) {
                TagLib::FLAC::Picture::Type ty = (*it)->type();
                switch((*it)->type()) {
                    case TagLib::FLAC::Picture::Other:
                        result_pic = *it;
                        break;
                    default:
                        break;
                }
            }
        }
        if(result_pic) {
            data_length = result_pic->data().size();
            data = new char[data_length];
            memcpy(data, result_pic->data().data(), data_length);
        }
    }
    
    if(!(data) || (data_length <= 0)) {
        return false;
    }
    return true;
}

bool FlacInfo::set_image(char* data, int data_length, ImageType image_type) {
    return false;
}


bool FlacInfo::can_handle_lyrics(void) {
    return true;
}


String FlacInfo::get_lyrics(void) {
    return get_xiph_comment_lyrics(m_XiphComment);
}


bool FlacInfo::set_lyrics(const String &lyrics) {
    return set_xiph_comment_lyrics(m_XiphComment, lyrics);
}


