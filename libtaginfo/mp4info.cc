/* Original Author 2008-2012: J.Rios
 * 
 * Edited by: 2012-2013 Jörn Magens <shuerhaaken@googlemail.com>
 * 
 * 
 * This Program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */


#include "taginfo.h"
#include "taginfo_internal.h"


using namespace TagInfo;



Mp4Info::Mp4Info(const string &filename) : Info(filename) {
    if(!file_name.isEmpty() && !create_file_ref())
        printf("Error creating file ref! %s\n", filename.c_str());
    if(taglib_file) {
        m_Mp4Tag = ((TagLib::MP4::File *) taglib_file)->tag();
    }
    else {
        printf("Cant get tag from '%s'\n", file_name.toCString(false));
        m_Mp4Tag = NULL;
    }
//    if(!file_name.isEmpty() && !create_file_ref())
//        printf("Error creating file ref! %s\n", filename.c_str());
//    if(taglib_fileref && !taglib_fileref->isNull()) {
//            m_Mp4Tag = ((TagLib::MP4::File *) taglib_fileref->file())->tag();
//    }
//    else
//        m_Mp4Tag = NULL;
}


Mp4Info::~Mp4Info() {
}


bool Mp4Info::create_file_ref() {
    if(file_name.isEmpty())
        return false;
    taglib_file = new TagLib::MP4::File(file_name.toCString(false), true, TagLib::AudioProperties::Fast);
    if(taglib_file) {
        return true;
    }
    else {
        printf("TagLib::File creation failed for '%s'\n", file_name.toCString(false));
        return false;
    }
}


bool Mp4Info::read(void) {
    if(Info::read()) {
        if(m_Mp4Tag) {
            read_virtual_tags((TagLib::Tag *)m_Mp4Tag);
            if(m_Mp4Tag->itemListMap().contains("aART")) {
                album_artist = m_Mp4Tag->itemListMap()["aART"].toStringList().front();
            }
            if(m_Mp4Tag->itemListMap().contains("\xA9wrt")) {
                composer = m_Mp4Tag->itemListMap()["\xa9wrt"].toStringList().front();
            }
            if(m_Mp4Tag->itemListMap().contains("disk")) {
                disk_str = format("%i/%i", m_Mp4Tag->itemListMap()["disk"].toIntPair().first,
                                           m_Mp4Tag->itemListMap()["disk"].toIntPair().second).c_str();
            }
            if(m_Mp4Tag->itemListMap().contains("cpil")) {
                is_compilation = m_Mp4Tag->itemListMap()["cpil"].toBool();
            }
            // Rating
            if(m_Mp4Tag->itemListMap().contains("----:com.apple.iTunes:RATING")) {
                long Rating = 0;
                Rating = atol(m_Mp4Tag->itemListMap()["----:com.apple.iTunes:RATING"].toStringList().front().toCString(true));
                if(Rating) {
                    if(Rating > 5) {
                        rating = popularity_to_rating(Rating);
                    }
                    else {
                        rating = Rating;
                    }
                }
            }
            if(m_Mp4Tag->itemListMap().contains("----:com.apple.iTunes:PLAY_COUNTER")) {
                long PlayCount = 0;
                PlayCount = atol(m_Mp4Tag->itemListMap()["----:com.apple.iTunes:PLAY_COUNTER"].toStringList().front().toCString(true));
                playcount = PlayCount;
            }
            // Labels
            if(track_labels.size() == 0) {
                if(m_Mp4Tag->itemListMap().contains("----:com.apple.iTunes:TRACK_LABELS")) {
                    track_labels_str = m_Mp4Tag->itemListMap()["----:com.apple.iTunes:TRACK_LABELS"].toStringList().front();
                    track_labels = split(track_labels_str, "|");
                }
            }
            if(artist_labels.size() == 0) {
                if(m_Mp4Tag->itemListMap().contains("----:com.apple.iTunes:ARTIST_LABELS")) {
                    artist_labels_str = m_Mp4Tag->itemListMap()["----:com.apple.iTunes:ARTIST_LABELS"].toStringList().front();
                    artist_labels = split(artist_labels_str, "|");
                }
            }
            if(album_labels.size() == 0) {
                if(m_Mp4Tag->itemListMap().contains("----:com.apple.iTunes:ALBUM_LABELS")) {
                    album_labels_str = m_Mp4Tag->itemListMap()["----:com.apple.iTunes:ALBUM_LABELS"].toStringList().front();
                    album_labels = split(album_labels_str, "|");
                }
            }
            if(m_Mp4Tag->itemListMap().contains("covr")) {
                TagLib::MP4::CoverArtList covers = m_Mp4Tag->itemListMap()[ "covr" ].toCoverArtList();
                has_image = (covers.size() > 0);
            }
            return true;
        }
    }
    return false;
}


void mp4_check_label_frame(TagLib::MP4::Tag * mp4tag, const char * description, const String &value) {
    if(mp4tag->itemListMap().contains(description)) {
            if(!value.isEmpty()) {
            mp4tag->itemListMap()[ description ] = TagLib::MP4::Item(TagLib::StringList(value));
        }
        else {
            mp4tag->itemListMap().erase(description);
        }
    }
    else {
            if(!value.isEmpty()) {
            mp4tag->itemListMap().insert(description, TagLib::MP4::Item(TagLib::StringList(value)));
        }
    }
}


bool Mp4Info::write(void) {
    if(m_Mp4Tag) {
        if(changedflag & CHANGED_DATA_TAGS) {
            m_Mp4Tag->itemListMap()["aART"] = TagLib::StringList(album_artist);
            m_Mp4Tag->itemListMap()["\xA9wrt"] = TagLib::StringList(composer);
            int first;
            int second;
            string_disk_to_disk_num(disk_str.toCString(true), first, second);
            m_Mp4Tag->itemListMap()["disk"] = TagLib::MP4::Item(first, second);
            m_Mp4Tag->itemListMap()["cpil"] = TagLib::MP4::Item(is_compilation);
            write_virtual_tags((TagLib::Tag *)m_Mp4Tag);
        }
        
        if(changedflag & CHANGED_DATA_RATING) {
            m_Mp4Tag->itemListMap()["----:com.apple.iTunes:RATING" ] = 
                TagLib::MP4::Item(format("%u", rating_to_popularity(rating)).c_str());
            
            m_Mp4Tag->itemListMap()[ "----:com.apple.iTunes:PLAY_COUNTER" ] = 
                TagLib::MP4::Item(format("%u", playcount).c_str());
        }
        
        if(changedflag & CHANGED_DATA_LABELS) {
            // The Labels
            mp4_check_label_frame(m_Mp4Tag, "----:com.apple.iTunes:ARTIST_LABELS", artist_labels_str);
            mp4_check_label_frame(m_Mp4Tag, "----:com.apple.iTunes:ALBUM_LABELS", album_labels_str);
            mp4_check_label_frame(m_Mp4Tag, "----:com.apple.iTunes:TRACK_LABELS", track_labels_str);
        }
    }
    return Info::write();
}

bool Mp4Info::can_handle_images(void) {
    return true;
}

bool Mp4Info::get_image(char*& data, int &data_length, ImageType &image_type) {
    if(m_Mp4Tag) {
        return get_mp4_cover_art(m_Mp4Tag, data, data_length, image_type);
    }
    return false;
}

bool Mp4Info::set_image(char* data, int data_length, ImageType image_type) {
    return false;
}

bool Mp4Info::can_handle_lyrics(void) {
    return true;
}


String Mp4Info::get_lyrics(void) {
    return get_mp4_lyrics(((TagLib::MP4::File *) taglib_fileref->file())->tag());
}


bool Mp4Info::set_lyrics(const String &lyrics) {
    return set_mp4_lyrics(((TagLib::MP4::File *) taglib_fileref->file())->tag(), lyrics);
}




