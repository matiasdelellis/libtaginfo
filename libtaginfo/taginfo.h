/* Original Author 2008-2012: J.Rios
 * 
 * Edited by: 2012-2013 Jörn Magens <shuerhaaken@googlemail.com>
 * 
 * 
 * This Program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

#ifndef TAGINFO_H
#define TAGINFO_H

#include <string>
#include <iostream>
#include "ape.h"

#include <tag.h>
#include <fileref.h>
#include <asffile.h>
#include <mp4file.h>
#include <oggfile.h>
#include <xiphcomment.h>

#include <apetag.h>
#include <id3v2tag.h>

#define NOT_FOUND -1

using namespace TagLib;
using namespace std;





namespace TagInfo {
    
    enum MediaFileType {
        MEDIA_FILE_TYPE_AAC,
        MEDIA_FILE_TYPE_AIF,
        MEDIA_FILE_TYPE_APE,
        MEDIA_FILE_TYPE_ASF,
        MEDIA_FILE_TYPE_FLAC,
        MEDIA_FILE_TYPE_M4A,
        MEDIA_FILE_TYPE_M4B,
        MEDIA_FILE_TYPE_M4P,
        MEDIA_FILE_TYPE_MP3,
        MEDIA_FILE_TYPE_MP4,
        MEDIA_FILE_TYPE_MPC,
        MEDIA_FILE_TYPE_OGA,
        MEDIA_FILE_TYPE_OGG,
        MEDIA_FILE_TYPE_TTA,
        MEDIA_FILE_TYPE_WAV,
        MEDIA_FILE_TYPE_WMA,
        MEDIA_FILE_TYPE_WV
    };
    
    enum ChangedData {
        CHANGED_DATA_NONE        = 0,
        CHANGED_ARTIST_TAG       = (1 << 0),
        CHANGED_ALBUM_TAG        = (1 << 1),
        CHANGED_TITLE_TAG        = (1 << 2),
        CHANGED_GENRE_TAG        = (1 << 3),
        CHANGED_COMMENT_TAG      = (1 << 4),
        CHANGED_TRACKNO_TAG      = (1 << 5),
        CHANGED_YEAR_TAG         = (1 << 6),
        CHANGED_DATA_ALBUMARTIST = (1 << 7),
        CHANGED_DATA_DISK_STR    = (1 << 8),
        CHANGED_DATA_IMAGES      = (1 << 9),
        CHANGED_DATA_LYRICS      = (1 << 10),
        CHANGED_DATA_LABELS      = (1 << 11),
        CHANGED_DATA_RATING      = (1 << 11),
        CHANGED_COMPOSER_TAG     = (1 << 12), // TODO: Reoreder them.
        CHANGED_DATA_TAGS        = (1 << 13) // Kept for compatibility: TODO: Remove
    };
    
    enum ImageType {
        IMAGE_TYPE_UNKNOWN,
        IMAGE_TYPE_JPEG,
        IMAGE_TYPE_PNG
    };
    
    
    class Info {
        protected :
            TagLib::FileRef * taglib_fileref;
            TagLib::File *    taglib_file;
            TagLib::Tag *     taglib_tag;
            
            void set_file_name(const string &filename);
            void read_virtual_tags(TagLib::Tag * tag);
            void write_virtual_tags(TagLib::Tag * tag);
            
            virtual bool create_file_ref();
            
            
        public:
            //Audioproperties
            int length_seconds;
            int bitrate;
            int samplerate;
            int channels;
            
            //General stuff
            String file_name;
            String track_name;
            String genre;
            String artist;
            String album_artist;
            String album;
            String composer;
            String comments;
            int tracknumber;
            int year;
            
            //access functions
            String get_track_name(void);
            void   set_track_name(String new_track_name);
            
            String get_genre(void);
            void   set_genre(String new_genre);
            
            String get_artist(void);
            void   set_artist(String new_artist);
            
            String get_album_artist(void);
            void   set_album_artist(String new_artist);
            
            String get_album(void);
            void   set_album(String new_album);
            
            String get_composer(void);
            void   set_composer(String new_composer);
            
            String get_comments(void);
            void   set_comments(String new_comments);
            
            int  get_tracknumber();
            void set_tracknumber(int new_tracknumber);
            
            int  get_year();
            void set_year(int new_year);
            
            int get_length_seconds();
            int get_bitrate();
            int get_channels();
            int get_samplerate();
           
            //Extra
            int playcount;
            int  get_playcount();
            void set_playcount(int new_playcount);
            int rating;
            int  get_rating();
            void set_rating(int new_rating);
            
            String disk_str;
            String get_disk_string();
            void   set_disk_string(String new_disk_string);
            
            //Labels
            StringList track_labels;
            String track_labels_str;
            StringList artist_labels;
            String artist_labels_str;
            StringList album_labels;
            String album_labels_str;
            
            //Misc
            bool is_compilation;
            bool has_image;
            
            int changedflag;
            
            Info(const string &filename = "");
            ~Info();
            
            
            virtual bool read(void);
            virtual bool write(void);
            
            virtual bool can_handle_images(void);
            virtual bool get_image(char*& data, int &data_length, ImageType &image_type);
            virtual bool set_image(char* data, int data_length, ImageType image_type);

            virtual bool can_handle_lyrics(void);
            virtual String get_lyrics(void);
            virtual bool set_lyrics(const String &lyrics);
            
            static Info * create_tag_info_with_format(const string &file, MediaFileType format);
            static Info * create_tag_info(const string &file);
    };


    
    class Mp3Info : public Info {
        protected :
            ID3v2::Tag * taglib_tagId3v2;
            virtual bool create_file_ref();
            
        public :
            Mp3Info(const string &filename = "");
            ~Mp3Info();
            
            virtual bool read(void);
            virtual bool write();
            
            virtual bool can_handle_images(void);
            virtual bool get_image(char*& data, int &data_length, ImageType &image_type);
            virtual bool set_image(char* data, int data_length, ImageType image_type);
            
            virtual bool can_handle_lyrics(void);
            virtual String get_lyrics(void);
            virtual bool set_lyrics(const String &lyrics);
    };
    
    class FlacInfo : public Info {
        protected :
            Ogg::XiphComment * m_XiphComment;
            virtual bool create_file_ref();
        
        public :
            FlacInfo(const string &filename = "");
            ~FlacInfo();
            
            virtual bool read(void);
            virtual bool write(void);
            
            virtual bool can_handle_images(void);
            virtual bool get_image(char*& data, int &data_length, ImageType &image_type);
            virtual bool set_image(char* data, int data_length, ImageType image_type);
            
            virtual bool can_handle_lyrics(void);
            virtual String get_lyrics(void);
            virtual bool set_lyrics(const String &lyrics);
    };
    
    
    class OggInfo : public Info {
        protected :
            Ogg::XiphComment * m_XiphComment;
            virtual bool create_file_ref();
            
        public :
            OggInfo(const string &filename = "");
            ~OggInfo();
            
            virtual bool read(void);
            virtual bool write(void);
            
            virtual bool can_handle_images(void);
            virtual bool get_image(char*& data, int &data_length, ImageType &image_type);
            virtual bool set_image(char* data, int data_length, ImageType image_type);
            
            virtual bool can_handle_lyrics(void);
            virtual String get_lyrics(void);
            virtual bool set_lyrics(const String &lyrics);
    };
    
    
    class Mp4Info : public Info {
        protected :
            TagLib::MP4::Tag * m_Mp4Tag;
            virtual bool create_file_ref();
            
        public :
            Mp4Info(const string &filename = "");
            ~Mp4Info();
            
            virtual bool read(void);
            virtual bool write(void);
            
            virtual bool can_handle_images(void);
            virtual bool get_image(char*& data, int &data_length, ImageType &image_type);
            virtual bool set_image(char* data, int data_length, ImageType image_type);
            
            virtual bool can_handle_lyrics(void);
            virtual String get_lyrics(void);
            virtual bool set_lyrics(const String &lyrics);
    };
    
    
    class ApeInfo : public Info {
        protected:
            Ape::ApeFile ape_file;
            virtual bool create_file_ref();
            
        public:
            ApeInfo(const string &filename = "");
            ~ApeInfo();
            
            virtual bool read(void);
            virtual bool write(void);
            
            virtual bool can_handle_lyrics(void);
            virtual String get_lyrics(void);
            virtual bool set_lyrics(const String &lyrics);
    };
    
    
    class MpcInfo : public Info {
        protected :
            TagLib::APE::Tag * taglib_apetag;
            virtual bool create_file_ref();
            
        public :
            MpcInfo(const string &filename = "");
            ~MpcInfo();
            
            virtual bool read(void);
            virtual bool write(void);
            
            virtual bool can_handle_images(void);
            virtual bool get_image(char*& data, int &data_length, ImageType &image_type);
            virtual bool set_image(char* data, int data_length, ImageType image_type);
    };
    
    
    class WavPackInfo : public Info {
        protected :
            TagLib::APE::Tag * taglib_apetag;
            virtual bool create_file_ref();
            
        public :
            WavPackInfo(const string &filename = "");
            ~WavPackInfo();
            
            virtual bool read(void);
            virtual bool write(void);
            
            virtual bool can_handle_images(void);
            virtual bool get_image(char*& data, int &data_length, ImageType &image_type);
            virtual bool set_image(char* data, int data_length, ImageType image_type);
            
            virtual bool can_handle_lyrics(void);
            virtual String get_lyrics(void);
            virtual bool set_lyrics(const String &lyrics);
    };
    
    
    class TrueAudioInfo : public Info {
        protected :
            ID3v2::Tag * taglib_tagId3v2;
            virtual bool create_file_ref();
        
        public :
            TrueAudioInfo(const string &filename = "");
            ~TrueAudioInfo();
            
            virtual bool read(void);
            virtual bool write(void);
            
            virtual bool can_handle_images(void);
            virtual bool get_image(char*& data, int &data_length, ImageType &image_type);
            virtual bool set_image(char* data, int data_length, ImageType image_type);
            
            virtual bool can_handle_lyrics(void);
            virtual String get_lyrics(void);
            virtual bool set_lyrics(const String &lyrics);
    };
    
    
    class ASFInfo : public Info {
        protected :
            ASF::Tag * m_ASFTag;
            virtual bool create_file_ref();
        
        public :
            ASFInfo(const string &filename = "");
            ~ASFInfo();
            
            virtual bool read(void);
            virtual bool write(void);
            
            virtual bool can_handle_images(void);
            virtual bool get_image(char*& data, int &data_length, ImageType &image_type);
            virtual bool set_image(char* data, int data_length, ImageType image_type);
            
            virtual bool can_handle_lyrics(void);
            virtual String get_lyrics(void);
            virtual bool set_lyrics(const String &lyrics);
    };
}
#endif
