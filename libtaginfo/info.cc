/* Original Author 2008-2012: J.Rios
 * 
 * Edited by: 2012-2013 Jörn Magens <shuerhaaken@googlemail.com>
 * 
 * 
 * This Program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

#include <algorithm>
#include <string> 
#include "taginfo.h"
#include "taginfo_internal.h"



using namespace TagInfo;

Info * Info::create_tag_info_with_format(const string & filename, MediaFileType format) {
    switch(format) {
        case  MEDIA_FILE_TYPE_MP3 :
            return new Mp3Info(filename);
        case  MEDIA_FILE_TYPE_FLAC :
            return new FlacInfo(filename);
        case  MEDIA_FILE_TYPE_OGG :
        case  MEDIA_FILE_TYPE_OGA :
            return new OggInfo(filename);
        case  MEDIA_FILE_TYPE_MP4 :
        case  MEDIA_FILE_TYPE_M4A :
        case  MEDIA_FILE_TYPE_M4B :
        case  MEDIA_FILE_TYPE_M4P :
        case  MEDIA_FILE_TYPE_AAC : 
            return new Mp4Info(filename);
        case  MEDIA_FILE_TYPE_WMA :
        case  MEDIA_FILE_TYPE_ASF :
            return new ASFInfo(filename);
        case MEDIA_FILE_TYPE_APE :
            return new ApeInfo(filename);
        case MEDIA_FILE_TYPE_WAV :
        case MEDIA_FILE_TYPE_AIF :
            return new Info(filename);
        case MEDIA_FILE_TYPE_WV : 
            return new WavPackInfo(filename);
        case MEDIA_FILE_TYPE_TTA :
            return new TrueAudioInfo(filename);
        case MEDIA_FILE_TYPE_MPC :
            return new MpcInfo(filename);
        default :
            break;
    }
    return NULL;
}

Info * Info::create_tag_info(const string & filename) {
    map<String,MediaFileType> ext_map;
    
    ext_map["MP3"]  = MEDIA_FILE_TYPE_MP3;
    ext_map["FLAC"] = MEDIA_FILE_TYPE_FLAC;
    ext_map["OGG"]  = MEDIA_FILE_TYPE_OGG;
    ext_map["OGA"]  = MEDIA_FILE_TYPE_OGA;
    ext_map["MP4"]  = MEDIA_FILE_TYPE_MP4;
    ext_map["M4A"]  = MEDIA_FILE_TYPE_M4A;
    ext_map["M4B"]  = MEDIA_FILE_TYPE_M4B;
    ext_map["M4P"]  = MEDIA_FILE_TYPE_M4P;
    ext_map["AAC"]  = MEDIA_FILE_TYPE_AAC;
    ext_map["WMA"]  = MEDIA_FILE_TYPE_WMA;
    ext_map["ASF"]  = MEDIA_FILE_TYPE_ASF;
    ext_map["APE"]  = MEDIA_FILE_TYPE_APE;
    ext_map["WAV"]  = MEDIA_FILE_TYPE_WAV;
    ext_map["AIF"]  = MEDIA_FILE_TYPE_AIF;
    ext_map["WV"]   = MEDIA_FILE_TYPE_WV;
    ext_map["TTA"]  = MEDIA_FILE_TYPE_TTA;
    ext_map["MPC"]  = MEDIA_FILE_TYPE_MPC;
    
    String fnex = filename.substr(filename.find_last_of(".") + 1).c_str();
    fnex = fnex.upper();
    
    if(ext_map[fnex] == 0)
        return NULL;
    
    MediaFileType format = ext_map[fnex];
    
    return Info::create_tag_info_with_format(filename, format);
}


// Info

Info::Info(const string &filename) {
    
    taglib_fileref = NULL;
    taglib_file    = NULL;
    taglib_tag     = NULL;
    
    set_file_name(filename);

    tracknumber = 0;
    year = 0;
    length_seconds = 0;
    bitrate = 0;
    rating = -1;
    playcount = 0;
    is_compilation = false;
    has_image = false;

	changedflag = CHANGED_DATA_NONE;
};


Info::~Info() {
    if(taglib_fileref)
        delete taglib_fileref;
    if(taglib_file)
        delete taglib_file;
}


void Info::set_file_name(const string &filename) {
    file_name = filename;
}


bool Info::create_file_ref() {
    if(!file_name.isEmpty())
        taglib_fileref = new TagLib::FileRef(file_name.toCString(false), true, TagLib::AudioProperties::Fast);
    else
        return false;
    
    if(taglib_fileref && !taglib_fileref->isNull()) {
        taglib_file = taglib_fileref->file();
        taglib_tag = taglib_file->tag();
        if(!taglib_tag) {
            printf("Cant get tag object from '%s'\n", file_name.toCString(false));
            return false;
        }
        return true;
    }
    else {
        return false;
    }
}


void Info::read_virtual_tags(TagLib::Tag * tag) {
    if(tag) {
        track_name  = tag->title();
        artist      = tag->artist();
        album       = tag->album();
        genre       = tag->genre();
        comments    = tag->comment();
        tracknumber = tag->track();
        year        = tag->year();
    }
}

bool Info::read(void) {
    AudioProperties * apro;
    if(taglib_tag) {
        read_virtual_tags(taglib_tag);
    }
    
    if(taglib_file && (apro = taglib_file->audioProperties())) {
        length_seconds = apro->length();
        bitrate = apro->bitrate();
        samplerate = apro->sampleRate();
        return true;
    }
    else if(taglib_fileref && (apro = taglib_fileref->audioProperties())) {
        length_seconds = apro->length();
        bitrate = apro->bitrate();
        //m_Samplerate = apro->sampleRate();
        return true;
    }
    //printf("Problem with Info::read for %s\n", file_name.toCString(false));
    return false;
}

void Info::write_virtual_tags(TagLib::Tag * tag) {
    if(changedflag & CHANGED_TITLE_TAG)
        tag->setTitle(track_name);
    if(changedflag & CHANGED_ARTIST_TAG)
        tag->setArtist(artist);
    if(changedflag & CHANGED_ALBUM_TAG)
        tag->setAlbum(album);
    if(changedflag & CHANGED_GENRE_TAG)
        tag->setGenre(genre);
    if(changedflag & CHANGED_COMMENT_TAG)
        tag->setComment(comments);
    if(changedflag & CHANGED_TRACKNO_TAG)
        tag->setTrack(tracknumber); // set the id3v1 track
    if(changedflag & CHANGED_YEAR_TAG)
        tag->setYear(year);
}

bool Info::write() {
    if(taglib_tag && changedflag)
        write_virtual_tags(taglib_tag);
    
    if(!taglib_file->save())
      return false;
    
    return true;
}

void Info::set_genre(String new_genre) {
    genre = new_genre;
    changedflag |= CHANGED_GENRE_TAG;
}
String Info::get_genre(void) {
    return genre;
}

void Info::set_artist(String new_artist) {
    artist = new_artist;
    changedflag |= CHANGED_ARTIST_TAG;
}
String Info::get_artist(void) {
    return artist;
}

void Info::set_album_artist(String new_album_artist) {
    album_artist = new_album_artist;
    changedflag |= CHANGED_DATA_ALBUMARTIST;
}
String Info::get_album_artist(void) {
    return album_artist;
}

void Info::set_album(String new_album) {
    album_artist = new_album;
    changedflag |= CHANGED_ALBUM_TAG;
}
String Info::get_album(void) {
    return album;
}

void Info::set_composer(String new_composer) {
    composer = new_composer;
    changedflag |= CHANGED_COMPOSER_TAG;
}
String Info::get_composer(void) {
    return composer;
}

void Info::set_tracknumber(int new_tracknumber) {
    tracknumber = new_tracknumber;
    changedflag |= CHANGED_TRACKNO_TAG;

}
int Info::get_tracknumber(void) {
    return tracknumber;
}

void Info::set_year(int new_year) {
    year = new_year;
    changedflag |= CHANGED_YEAR_TAG;

}
int Info::get_year(void) {
    return year;
}

bool Info::can_handle_images(void) {
    return false;
}

bool Info::get_image(char*& data, int &data_length, ImageType &image_type) {
    data = NULL;
    data_length = 0;
    image_type = IMAGE_TYPE_UNKNOWN;
    return false;
}

bool Info::set_image(char* data, int data_length, ImageType image_type) {
    return false;
}


bool Info::can_handle_lyrics(void) {
    return false;
}


String Info::get_lyrics(void) {
    return "";
}


bool Info::set_lyrics(const String &lyrics) {
    return false;
}










// Other functions

//wxImage * guTagGetPicture(const String &filename)
//{
//    wxImage * RetVal = NULL;
//    Info * TagInfo = create_tag_info(filename);
//    if(TagInfo)
//    {
//        if(TagInfo->can_handle_images())
//        {
//            RetVal = TagInfo->get_image();
//        }
//        delete TagInfo;
//    }
//    return RetVal;
//}


//bool guTagSetPicture(const String &filename, wxImage * picture)
//{
//    guMainFrame * MainFrame = (guMainFrame *) wxTheApp->GetTopWindow();

//    const guCurrentTrack * CurrentTrack = MainFrame->GetCurrentTrack();
//    if(CurrentTrack && CurrentTrack->m_Loaded)
//    {
//        if(CurrentTrack->file_name == filename)
//        {
//            // Add the pending track change to MainFrame
//            MainFrame->AddPendingUpdateTrack(filename, picture, "", guTRACK_CHANGED_DATA_IMAGES);
//            return true;
//        }
//    }

//    bool RetVal = false;
//    Info * TagInfo = create_tag_info(filename);
//    if(TagInfo)
//    {
//        if(TagInfo->can_handle_images())
//        {
//            RetVal = TagInfo->set_image(picture) && TagInfo->write(guTRACK_CHANGED_DATA_IMAGES);
//        }
//        delete TagInfo;
//    }
//    return RetVal;
//    return false;
//}


//bool guTagSetPicture(const String &filename, const String &imagefile)
//{
//    wxImage Image(imagefile);
//    if(Image.IsOk())
//    {
//        return guTagSetPicture(filename, &Image);
//    }
//    return false;
//}


//String guTagget_lyrics(const String &filename)
//{
//    String RetVal = "";
////    Info * TagInfo = create_tag_info(filename);
////    if(TagInfo)
////    {
////        if(TagInfo->can_handle_lyrics())
////        {
////            RetVal = TagInfo->get_lyrics();
////        }
////        delete TagInfo;
////    }
//    return RetVal;
//}

//
//bool guTagset_lyrics(const String &filename, String &lyrics)
//{
////    guMainFrame * MainFrame = (guMainFrame *) wxTheApp->GetTopWindow();

////    const guCurrentTrack * CurrentTrack = MainFrame->GetCurrentTrack();
////    if(CurrentTrack && CurrentTrack->m_Loaded)
////    {
////        if(CurrentTrack->file_name == filename)
////        {
////            // Add the pending track change to MainFrame
////            MainFrame->AddPendingUpdateTrack(filename, NULL, lyrics, guTRACK_CHANGED_DATA_LYRICS);
////            return true;
////        }
////    }

////    bool RetVal = false;
////    Info * TagInfo = create_tag_info(filename);
////    if(TagInfo)
////    {
////        if(TagInfo->can_handle_lyrics())
////        {
////            RetVal = TagInfo->set_lyrics(lyrics) && TagInfo->write(guTRACK_CHANGED_DATA_LYRICS);
////        }
////        delete TagInfo;
////    }
////    return RetVal;
//    return false;
//}


//void guUpdateTracks(const guTrackArray &tracks, const guImagePtrArray &images,
//                    const wxArrayString &lyrics, const wxArrayInt &changedflags)
//{
//    int Index;
//    int Count = tracks.size();

//    guMainFrame * MainFrame = guMainFrame::GetMainFrame();

//    // Process each Track
//    for(Index = 0; Index < Count; Index++)
//    {
//        // If there is nothign to change continue with next one
//        int ChangedFlag = changedflags[ Index ];
//        if(!ChangedFlag)
//            continue;

//        const guTrack &Track = tracks[ Index ];

//        // Dont allow to edit tags from Cue files tracks
//        if(Track.m_Offset)
//            continue;

//        if(wxFileExists(Track.file_name))
//        {
//            // Prevent write to the current playing file in order to avoid segfaults specially with flac and wma files
//            const guCurrentTrack * CurrentTrack = MainFrame->GetCurrentTrack();
//            if(CurrentTrack && CurrentTrack->m_Loaded)
//            {
//                if(CurrentTrack->file_name == Track.file_name)
//                {
//                    // Add the pending track change to MainFrame
//                    MainFrame->AddPendingUpdateTrack(Track,
//                                                       Index < (int) images.size() ? images[ Index ] : NULL,
//                                                       Index < (int) lyrics.size() ? lyrics[ Index ] : wxT(""),
//                                                       changedflags[ Index ]);
//                    continue;
//                }
//            }

//            Info * TagInfo = create_tag_info(Track.file_name);

//            if(!TagInfo)
//            {
//                guLogError(wxT("There is no handler for the file '%s'"), Track.file_name.c_str());
//                return;
//            }

//            if(ChangedFlag & CHANGED_DATA_TAGS)
//            {
//                TagInfo->track_name = Track.m_SongName;
//                TagInfo->album_artist = Track.album_artist;
//                TagInfo->artist = Track.artist;
//                TagInfo->album = Track.album;
//                TagInfo->genre = Track.genre;
//                TagInfo->tracknumber = Track.m_Number;
//                TagInfo->year = Track.year;
//                TagInfo->composer = Track.composer;
//                TagInfo->comments = Track.comments;
//                TagInfo->disk_str = Track.disk_str;
//            }

//            if(ChangedFlag & guTRACK_CHANGED_DATA_RATING)
//            {
//                TagInfo->rating = Track.rating;
//                TagInfo->playcount = Track.playcount;
//            }

//            if((ChangedFlag & guTRACK_CHANGED_DATA_LYRICS) && TagInfo->can_handle_lyrics())
//            {
//                TagInfo->set_lyrics(lyrics[ Index ]);
//            }

//            if((ChangedFlag & guTRACK_CHANGED_DATA_IMAGES) && TagInfo->can_handle_images())
//            {
//                TagInfo->set_image(images[ Index ]);
//            }

//            TagInfo->write(ChangedFlag);

//            delete TagInfo;
//        }
//        else
//        {
//            guLogMessage(wxT("File not found for edition: '%s'"), Track.file_name.c_str());
//        }
//    }
//}


//void guUpdateImages(const guTrackArray &songs, const guImagePtrArray &images, const wxArrayInt &changedflags)
//{
//    int Index;
//    int Count = images.size();
//    for(Index = 0; Index < Count; Index++)
//    {
//        if(!songs[ Index ].m_Offset && (changedflags[ Index ] & guTRACK_CHANGED_DATA_IMAGES))
//            guTagSetPicture(songs[ Index ].file_name, images[ Index ]);
//    }
//}


//void guUpdateLyrics(const guTrackArray &songs, const wxArrayString &lyrics, const wxArrayInt &changedflags)
//{
//    int Index;
//    int Count = lyrics.size();
//    for(Index = 0; Index < Count; Index++)
//    {
//        if(!songs[ Index ].m_Offset && (changedflags[ Index ] & guTRACK_CHANGED_DATA_LYRICS))
//            guTagset_lyrics(songs[ Index ].file_name, lyrics[ Index ]);
//    }
//}

