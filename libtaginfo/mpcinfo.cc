/* Original Author 2008-2012: J.Rios
 * 
 * Edited by: 2012-2013 Jörn Magens <shuerhaaken@googlemail.com>
 * 
 * 
 * This Program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */


#include "taginfo.h"
#include "taginfo_internal.h"


using namespace TagInfo;



MpcInfo::MpcInfo(const string &filename) : Info(filename) {
    if(!file_name.isEmpty() && !create_file_ref())
        printf("Error creating file ref! %s\n", filename.c_str());
    if(taglib_file) {
        taglib_apetag = ((TagLib::MPC::File *) taglib_file)->APETag();
    }
    else {
        printf("Cant get tag from '%s'\n", file_name.toCString(false));
        taglib_apetag = NULL;
    }
//    if(!file_name.isEmpty() && !create_file_ref())
//        printf("Error creating file ref! %s\n", filename.c_str());
//    if(taglib_fileref && !taglib_fileref->isNull())
//        taglib_apetag = ((TagLib::MPC::File *) taglib_fileref->file())->APETag();
//    else
//        taglib_apetag = NULL;
}


MpcInfo::~MpcInfo() {
}


bool MpcInfo::create_file_ref() {
    if(file_name.isEmpty())
        return false;
    taglib_file = new TagLib::MPC::File(file_name.toCString(false), true, TagLib::AudioProperties::Fast);
    if(taglib_file) {
        return true;
    }
    else {
        printf("TagLib::File creation failed for '%s'\n", file_name.toCString(false));
        return false;
    }
//    if(!file_name.isEmpty())
//        taglib_fileref = new TagLib::FileRef(file_name.toCString(false), true, TagLib::AudioProperties::Fast);
//    else
//        return false;
//    
//    if(taglib_fileref && !taglib_fileref->isNull()) {
//        taglib_tag = taglib_fileref->tag();
//        if(!taglib_tag) {
//            printf("Cant get tag object from '%s'\n", file_name.toCString(false));
//            return false;
//        }
//        return true;
//    }
//    else {
//        return false;
//    }
}


bool MpcInfo::can_handle_images(void) {
    return true;
}

bool MpcInfo::get_image(char*& data, int &data_length, ImageType &image_type) {
    if(taglib_apetag) {
        return get_ape_image(taglib_apetag, data, data_length, image_type);
    }
    return false;
}

bool MpcInfo::set_image(char* data, int data_length, ImageType image_type) {
    return false;
}

bool MpcInfo::read(void) {
    if(Info::read()) {
        if(taglib_apetag) {
            read_virtual_tags((TagLib::Tag *)taglib_apetag);
            if(taglib_apetag->itemListMap().contains("COMPOSER")) {
                composer = taglib_apetag->itemListMap()["COMPOSER"].toStringList().front();
            }
            if(taglib_apetag->itemListMap().contains("DISCNUMBER")) {
                disk_str = taglib_apetag->itemListMap()["DISCNUMBER"].toStringList().front();
            }
            if(taglib_apetag->itemListMap().contains("COMPILATION")) {
                is_compilation = taglib_apetag->itemListMap()["COMPILATION"].toStringList().front() ==  String("1");
            }
            if(taglib_apetag->itemListMap().contains("ALBUM ARTIST")) {
                album_artist = taglib_apetag->itemListMap()["ALBUM ARTIST"].toStringList().front();
            }
            else if(taglib_apetag->itemListMap().contains("ALBUMARTIST")) {
                album_artist = taglib_apetag->itemListMap()["ALBUMARTIST"].toStringList().front();
            }
            // Rating
            if(taglib_apetag->itemListMap().contains("RATING")) {
                long Rating = 0;
                Rating = atol(taglib_apetag->itemListMap()["RATING"].toStringList().front().toCString(true));
                if(Rating) {
                    if(Rating > 5) {
                        rating = popularity_to_rating(Rating);
                    }
                    else {
                        rating = Rating;
                    }
                }
            }
            if(taglib_apetag->itemListMap().contains("PLAY_COUNTER")) {
                long PlayCount = 0;
                PlayCount = atol(taglib_apetag->itemListMap()["PLAY_COUNTER"].toStringList().front().toCString(true));
                playcount = PlayCount;
            }
            // Labels
            if(track_labels.size() == 0) {
                if(taglib_apetag->itemListMap().contains("TRACK_LABELS")) {
                    track_labels_str = taglib_apetag->itemListMap()["TRACK_LABELS"].toStringList().front();
                    track_labels = split(track_labels_str, "|");
//                    track_labels = Regex::split_simple("|", track_labels_str);//(track_labels_str, wxT("|"));
                }
            }
            if(artist_labels.size() == 0) {
                if(taglib_apetag->itemListMap().contains("ARTIST_LABELS")) {
                    artist_labels_str = taglib_apetag->itemListMap()["ARTIST_LABELS"].toStringList().front();
                    artist_labels = split(artist_labels_str, "|");
                }
            }
            if(album_labels.size() == 0) {
                if(taglib_apetag->itemListMap().contains("ALBUM_LABELS")) {
                    album_labels_str = taglib_apetag->itemListMap()["ALBUM_LABELS"].toStringList().front();
                    album_labels = split(album_labels_str, "|");
                }
            }
            if(taglib_apetag->itemListMap().contains("Cover Art (front)")) {
                has_image = true;
            }
            else if(taglib_apetag->itemListMap().contains("Cover Art (other)")) {
                has_image = true;
            }
            return true;
        }
    }
    return false;
}


bool MpcInfo::write(void) {
    if(taglib_apetag) {
        if(changedflag & CHANGED_DATA_TAGS) {
            taglib_apetag->addValue("COMPOSER", composer);
            taglib_apetag->addValue("DISCNUMBER", disk_str);
            
            if(is_compilation) {
                taglib_apetag->addValue("COMPILATION", "1");
            }
            else {
                taglib_apetag->addValue("COMPILATION", "0");
            }
            taglib_apetag->addValue("ALBUM ARTIST", album_artist);
            
            write_virtual_tags((TagLib::Tag *)taglib_apetag);
        }
        if(changedflag & CHANGED_DATA_RATING) {
            char* str;
            
            if(asprintf (&str, "%u", rating_to_popularity(rating)) >= 0) { //TODO
                taglib_apetag->addValue("RATING", str);
                free (str);
                str = NULL;
            }
            
            if(asprintf (&str, "%u", playcount) >= 0) {
                taglib_apetag->addValue("PLAY_COUNTER", str);
                free (str);
            }
        }
        if(changedflag & CHANGED_DATA_LABELS) {
            // The Labels
            check_ape_label_frame(taglib_apetag, "ARTIST_LABELS", artist_labels_str);
            check_ape_label_frame(taglib_apetag, "ALBUM_LABELS",  album_labels_str);
            check_ape_label_frame(taglib_apetag, "TRACK_LABELS",  track_labels_str);
        }
    }
    return Info::write();
}



