/* Original Author 2008-2012: J.Rios
 * 
 * Edited by: 2012-2013 Jörn Magens <shuerhaaken@googlemail.com>
 * 
 * 
 * This Program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file LICENSE.  If not, write to
 * the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 * http://www.gnu.org/copyleft/gpl.html
 */

#include <string>
#include "taginfo.h"
#include "taginfo_internal.h"
#include <mp4file.h>


bool get_mp4_cover_art(TagLib::MP4::Tag * mp4tag, char*& data, int &data_length, ImageType &image_type) {
    data = NULL;
    data_length = 0;
    image_type = IMAGE_TYPE_UNKNOWN;
    
    if(mp4tag && mp4tag->itemListMap().contains("covr")) {
        TagLib::MP4::CoverArtList Covers = mp4tag->itemListMap()[ "covr" ].toCoverArtList();
        
        for(TagLib::MP4::CoverArtList::Iterator it = Covers.begin(); it != Covers.end(); it++) {
            if(it->format() == TagLib::MP4::CoverArt::PNG) {
                image_type = IMAGE_TYPE_PNG;
            }
            else if(it->format() == TagLib::MP4::CoverArt::JPEG) {
                image_type = IMAGE_TYPE_JPEG;
            }
            data_length = it->data().size();
            data = new char[data_length];
            memcpy(data, it->data().data(), it->data().size());
            return true;
        }
        return false;
    }
    return false;
}


String get_mp4_lyrics(TagLib::MP4::Tag * mp4tag) {
    if(mp4tag) {
            if(mp4tag->itemListMap().contains("\xa9lyr"))
            return mp4tag->itemListMap()[ "\xa9lyr" ].toStringList().front();
    }
    return "";
}


bool set_mp4_lyrics(TagLib::MP4::Tag * mp4tag, const String &lyrics) {
    if(mp4tag) {
            if(mp4tag->itemListMap().contains("\xa9lyr")) {
            mp4tag->itemListMap().erase("\xa9lyr");
        }
        if(!lyrics.isEmpty()) {
            mp4tag->itemListMap()[ "\xa9lyr" ] = TagLib::StringList(lyrics);
        }
        return true;
    }
    return false;
}



