using TagInfo;


void main(string[] args) {
    Gtk.init(ref args);
    Info info = Info.factory_make("/path/to/file.flac");
    if(!info.read()) {
        print("error read\n");
        return;
    }
    Gdk.Pixbuf pixbuf = null;
    uint8[] data = null;
    if(!info.get_image(out data)) {
        print("error 0\n");
        return;
    }
    assert(data != null);
    var pbloader = new Gdk.PixbufLoader();
    try {
        uint8[] idata = (uint8[])data;
        idata.length = data.length;
        pbloader.write(idata);
    }
    catch(Error e) {
        print("Error 1: %s\n", e.message);
        try { pbloader.close(); } catch(Error e) { print("Error 2\n");}
        return;
    }
    try { pbloader.close(); } catch(Error e) {}
    pixbuf = pbloader.get_pixbuf();
    assert(pixbuf != null);
    
    Gtk.Image image = new Gtk.Image.from_pixbuf(pixbuf);
    var win = new Gtk.Window();
    win.add(image);
    win.show_all();
    win.destroy.connect(Gtk.main_quit);
    Gtk.main();
    return;
}
