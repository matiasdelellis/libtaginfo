using TagInfo;


void main(string[] args) {
    Info info = Info.factory_make("path/to/file.mp3");
    if(!info.read()) {
        print("error read\n");
        return;
    }
    Gdk.Pixbuf pixbuf = new Gdk.Pixbuf.from_file("/path/to/image.jpeg");
    assert(pixbuf != null);
    uint8[] data;
    try {
        pixbuf.save_to_buffer(out data, "jpeg");
    }
    catch(Error e) {
        print("%s\n", e.message);
    }
    if(!info.set_image(data, ImageType.UNKNOWN)) {
        print("error 0\n");
        return;
    }
    info.write();
}
