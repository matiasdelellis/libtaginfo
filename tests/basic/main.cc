#include "taginfo.h"

using namespace TagInfo;

int main( void )
{
    Info * info;
    
    std::string val = TESTDIR "samples/sample.flac";
    //std::cout << std::endl << "val: " << val << std::endl;
    info = Info::create_tag_info( val );
    if( info )
    {
        delete info;
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}
