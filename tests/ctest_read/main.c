#include <stdio.h>
#include <taginfo_c.h>
#include <stdlib.h>

int main(void)
{
    TagInfo_Info *info;
    
    char val[200] = TESTDIR "samples/sample.flac";
    info = taginfo_info_factory_make(val);
    
    if(info == NULL)
        return 1; //EXIT_FAILURE
    
    if(taginfo_info_read(info)) {
        char* name = taginfo_info_get_title(info);
        free(name);
    }
    else {
        taginfo_info_free(info);
        return 1; //EXIT_FAILURE
    }
    taginfo_info_free(info);
    return 0; //EXIT_SUCCESS
}
