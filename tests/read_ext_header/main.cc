#include "taginfo.h"

using namespace TagInfo;

int main( void )
{
    Info * TagInfo;
    std::string val = TESTDIR "samples/sample_v2_3_ext_header.mp3";
    
    //std::cout << std::endl << "val: " << val << std::endl;
    
    TagInfo = Info::create_tag_info( val );
    if( TagInfo )
    {
        if( TagInfo->read() )
        {
            //std::cout << "TagInfo->track_name: " << TagInfo->track_name << std::endl;
            if(TagInfo->track_name == "Title v1") {
                delete TagInfo;
                return EXIT_SUCCESS;
            }
        }
        delete TagInfo;
        return EXIT_FAILURE;
    }
}
