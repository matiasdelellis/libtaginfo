#include "taginfo.h"

using namespace TagInfo;

int main( void )
{
    Info * TagInfo;
    std::string val = TESTDIR "samples/sample_v1_only.mp3";
    //std::cout << std::endl << "val: " << val << std::endl;
    
    TagInfo = Info::create_tag_info( val );
    if( TagInfo )
    {
        if( TagInfo->read() )
        {
            if(TagInfo->track_name == "MP3 title") {
                delete TagInfo;
                return EXIT_SUCCESS;
            }
        }
        delete TagInfo;
        return EXIT_FAILURE;
    }
}
