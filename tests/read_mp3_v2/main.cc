#include "taginfo.h"

using namespace TagInfo;

int main( void )
{
    Info * TagInfo;
    std::string val = TESTDIR "samples/sample_v2_only.mp3";
    //std::cout << std::endl << "val: " << val << std::endl;
    
    TagInfo = Info::create_tag_info( val );
    if( TagInfo )
    {
        if( TagInfo->read() )
        {
            //std::cout << "TagInfo->track_name: " << TagInfo->track_name << std::endl;
            //std::cout << "TagInfo->artist: " << TagInfo->artist << std::endl;
            //std::cout << "TagInfo->album: " << TagInfo->album << std::endl;
            if(TagInfo->track_name == "MP3 title") {
                delete TagInfo;
                return EXIT_SUCCESS;
            }
        }
        delete TagInfo;
        return EXIT_FAILURE;
    }
}
