#include "taginfo.h"
#include <stdio.h>
#include <iostream>
#include <fstream>

#define TESTNAME "xyz 1 23"

using namespace TagInfo;

int main( void )
{
    Info * TagInfo;
    std::string val = TESTDIR "samples/sample.mpc";
    //std::cout << std::endl << "val: " << val << std::endl;
    
    std::string target("/tmp/out_01.mpc");
    
    std::ifstream  src(val.c_str());
    std::ofstream  dst(target.c_str());
    dst << src.rdbuf();
    
    TagInfo = Info::create_tag_info(target);
    if( TagInfo )
    {
        TagInfo->track_name = TESTNAME;
        TagInfo->write(CHANGED_DATA_TAGS);
    }
    delete TagInfo;
    TagInfo = NULL;
    
    TagInfo = Info::create_tag_info(target);
    if( TagInfo )
    {
        if( TagInfo->read() )
        {
            if(TagInfo->track_name == TESTNAME) {
                delete TagInfo;
                if(remove(target.c_str()) != 0 )
                    return EXIT_FAILURE;
                return EXIT_SUCCESS;
            }
        }
        delete TagInfo;
        if(remove(target.c_str()) != 0 )
            return EXIT_FAILURE;
        return EXIT_FAILURE;
    }
    else {
        delete TagInfo;
        if(remove(target.c_str()) != 0 )
            return EXIT_FAILURE;
        return EXIT_FAILURE;
    }
}
