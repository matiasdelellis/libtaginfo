#include "taginfo.h"
#include <stdio.h>
#include <iostream>
#include <fstream>

using namespace TagInfo;


int main( void )
{
    Info * TagInfo;
    
    std::string val = TESTDIR "samples/sample.ape";
    //std::cout << std::endl << "val: " << val << std::endl;
    
    std::string target = "/tmp/out_01.ape";
    
    std::ifstream  src(val.c_str());
    std::ofstream  dst(target.c_str());
    dst << src.rdbuf();
     
    TagInfo = Info::create_tag_info(target);
    if( TagInfo )
    {
        if( TagInfo->read() )
        {
            //std::cout << "TagInfo->artist: " << TagInfo->artist << std::endl;
            if(TagInfo->track_name == "APE title" && TagInfo->artist == "APE artist") {
                delete TagInfo;
                if(remove(target.c_str()) != 0 )
                    return EXIT_FAILURE;
                return EXIT_SUCCESS;
            }
        }
        delete TagInfo;
        if(remove(target.c_str()) != 0 )
            return EXIT_FAILURE;
        return EXIT_FAILURE;
    }
}
