#include <stdio.h>
#include <taginfo_c.h>
#include <stdlib.h>


#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#define TESTTITLE "test title äöüß"

int cp(const char *from, const char *to)
{
    int fd_to, fd_from;
    char buf[4096];
    ssize_t nread;
    int saved_errno;
    
    fd_from = open(from, O_RDONLY);
    if (fd_from < 0)
        return -1;
    
    fd_to = open(to, O_WRONLY | O_CREAT , 0666); //| O_EXCL
    if (fd_to < 0)
        goto out_error;
    
    while (nread = read(fd_from, buf, sizeof buf), nread > 0)
    {
        char *out_ptr = buf;
        ssize_t nwritten;
        
        do {
            nwritten = write(fd_to, out_ptr, nread);
            
            if (nwritten >= 0)
            {
                nread -= nwritten;
                out_ptr += nwritten;
            }
            else if (errno != EINTR)
            {
                goto out_error;
            }
        } while (nread > 0);
    }
    
    if (nread == 0)
    {
        if (close(fd_to) < 0)
        {
            fd_to = -1;
            goto out_error;
        }
        close(fd_from);
        
        /* Success! */
        return 0;
    }
    
  out_error:
    saved_errno = errno;
    close(fd_from);
    if (fd_to >= 0)
        close(fd_to);
    
    errno = saved_errno;
    return -1;
}


int main(void)
{
    TagInfo_Info *info;
    
    char val[200] = TESTDIR "samples/sample.flac";
    const char* target = "/tmp/out_03.flac";
    const char* s = val;
    
    if(cp(s, target) < 0)
        return 1; //EXIT_FAILURE
    
    info = taginfo_info_factory_make(target);
    
    if(info == NULL)
        return 1; //EXIT_FAILURE
    
    taginfo_info_set_title(info, TESTTITLE);
    taginfo_info_write(info);
    taginfo_info_free(info);
    
    info = taginfo_info_factory_make(target);
    if(taginfo_info_read(info)) {
        char* name = taginfo_info_get_title(info);
        if(strcmp(name, TESTTITLE) == 0) {
            free(name);
            taginfo_info_free(info);
            return 0; //EXIT_SUCCESS
        }
        free(name);
        taginfo_info_free(info);
        return 1; //EXIT_FAILURE
    }
    else {
        taginfo_info_free(info);
        return 1; //EXIT_FAILURE
    }
}
