**TagInfo** is a library for reading media metadata (tags).


**TagInfo** is a convenience wrapper for taglib with **C** and **vala** bindings.

Features are reading/writing fields like:

 - Artist name

 - Album name

 - Track title

 - Genre

 - AlbumArtist

 - Comments

 - Disk number

 - Compilation flag

 - User labels

 - Images

 - Lyrics

 - Audio properties (length, bitrate, ...)

 - ...


Supported files: Mp3, Ogg, Flac, MPC, Ape, Speex, WavPack, TrueAudio, WAV, AIFF, MP4 and ASF.


**TagInfo** is quite fast. Very rough tests have shown that it is about 40 - 60 times faster in file reading that with GStreamer based file reading using the codecs (which obviously is very slow). 

**This library offers C and vala bindings that provide a lot of features that are not available via TagLib's own C bindings (and the according vala bindings).** 

**The binding situation was the original motivation for the creation of this library.**

***

**TagInfo** can be used from **C++, C and vala** so far.

***

This library is under development!

API might change, although it has been stabilized in the last months.

API will be extended.

A few functionalities are not implemented for some file types.



***

**PLEASE HELP IMPROVING TAGINFO!**

**I would appreciate to collect patches for this library** and improve it for the benefit of everybody that needs a convenient, fast (and toolkit independent) C / Vala API for tag reading/writing.

There is a **TODO** file in the project folder listing some things that need to be done.


***

 - This library has been developed by shuerhaaken for the **[xnoise media player](http://www.xnoise-media-player.com/)**.

 - Parts of the C++ code was extracted from the guayadeque media player and modified to avoid wxWidgets dependencies.

 - C bindings by shuerhaaken.

 - Vala bindings by shuerhaaken.



***

**If you have questions please ask on the [xnoise mailing list](http://groups.google.com/group/xnoise) !**

***


